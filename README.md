# Le projet
Ce n’est pas un projet à proprement parler, mais plutôt une collection de *cheat sheets*, c’est-à-dire des feuilles courtes rassemblant les commandes / trucs utiles à connaître sur un outil.
C’est une sorte d’aide-mémoire instantané, à la fois pour aider à l’apprentissage de ces outils, mais aussi pour se remémorer une commande en cas d’oubli.

À avoir donc toujours à portée de main !

# Le contenu
Une liste des CS, dont certaines sont à faire:
* [x] Vim
* [x] Emacs (non encore publiée, il faut que je la retrouve dans les méandres de mon DD externe
* [X] LaTeX (fiche basique pour le moment)
* [ ] Sed (syntaxe et options de la ligne de commande
* [ ] R
* [ ] Bash
